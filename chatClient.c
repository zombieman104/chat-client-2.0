#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/select.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>
#include <ncurses.h>

//Worked with Ryley

int main(){
	struct sockaddr_in serv; //Primary socket variable
	int fd;	//Socket file descriptor that identifies the socket
	fd_set rfds; 	//Array of file descriptors
	struct timeval timeout;
	int yMax, xMax;


	char message[100] = "";	//array that stores messages that are sent by the server
	char username[20];
	char buffer[80];
	
	initscr();		//Start curses mode
	cbreak();			// Disables line buffering 

	
	getmaxyx(stdscr, yMax, xMax);	//provides window dimensions

	/*create output window */
	WINDOW * outputwin = newwin(4, xMax, yMax, 5);
	box(outputwin, 0, 0);
	refresh();
	wrefresh(outputwin);

	/* create input window */
	WINDOW * inputwin = newwin(4, xMax-12, yMax-5, 5);
	box(inputwin, 0, 0);
	refresh();
	wrefresh(inputwin);
	
	fd = socket(AF_INET, SOCK_STREAM, 0);	//creates a new socket that returns the identifier of the socket to the file descriptor

	serv.sin_family = AF_INET;		//chooses IPv4
	serv.sin_port = htons(49153);	//defines the port at which the server will listen for connections

	inet_pton(AF_INET, "10.115.20.250", &serv.sin_addr); 	//binds the westmont pilot ip to the local host

	connect(fd, (struct sockaddr *)&serv, sizeof(serv)); //connects client to server

	printf("Press Enter to Start\n");

	wgetnstr(inputwin, username, 20);
			
	printf("Hi %s, I am connecting you to the server \n" ,username);
	
	if(send(fd, username, strlen(username),0)<0) { 		//sends username to server and checks for error
		printf("Failed to send\n");
	}
	
	printf("%s\n", "Insert Username Please");


	while(1){
		/* selection */
		FD_ZERO(&rfds);
		FD_SET(0, &rfds);				//Listens to the standard input
		FD_SET(fd, &rfds);				//Listens to the socket

		/* timeout waits 5 seconds */
		timeout.tv_sec = 5;
		timeout.tv_usec = 0;
		
		select((fd+1), &rfds, NULL, NULL, &timeout);

		/* selection handling */
		if (FD_ISSET(0, &rfds))		//reads the standard input
		{	
	
			/* Message capability */
			wgetnstr(inputwin, message, 80);

			int length = strlen(message);
			strcat(message, "\n");
			strcat(message, "\0");

			send(fd, message, strlen(message), 0);

			printf("%s\n", buffer);		//Prints data from the server when pressing enter		
		}
		
		
		if(FD_ISSET(fd, &rfds)) {		//reads the socket
			printf("No data within 5 seconds.\n");
			if(read(fd, buffer, 80)<0){ 		//Client reads data from the server
				printf("Failed to buffer\n");
			}	
			printf("%s\n", buffer);
		}

	}

	endwin();
	
}